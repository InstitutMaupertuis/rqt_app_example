#ifndef SIMPLE_QT_GUI_MAINWINDOW_HPP
#define SIMPLE_QT_GUI_MAINWINDOW_HPP

#include <QApplication>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QLabel>
#include <ros/ros.h>

namespace simple_qt_gui
{

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();

protected:
  ros::NodeHandle nh_;
};

}

#endif
